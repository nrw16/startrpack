#' add
#'
#' Add together two numbers.
#'
#' @name add
#' @param x The first number to be added.
#' @param y The second number to be added.
#' @return The sum of x and y.
#' @export
#' @examples
#' add(1,1)
#' add(1,0)
add <- function(x,y){return(x+y)}
