context("minus")

test_that("minus function gets correct results", {

  x <- 10; y <- 5
  expect_equal( minus(x,y), 5 )

  x <- 50; y <- 5
  expect_equal( minus(x,y), 45 )

})
