context("add")

test_that("add function gets correct results", {

  x <- 10; y <- 5
  expect_equal( add(x,y), 15 )

  x <- 50; y <- 5
  expect_equal( add(x,y), 55 )

})
