% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/makedata.R
\name{makedata}
\alias{makedata}
\title{makedata}
\usage{
makedata(ploc, robj = data.frame(A = c(1, 2, 3), B = c(3, 2, 1)),
  oname = "mydat", doc = FALSE)
}
\arguments{
\item{ploc}{The package location.}

\item{robj}{An R object in the current session to save into the rda file.}

\item{oname}{A character string to use as the name of the R object in the rda file.}
}
\value{
NULL (an R data object in an .rda file is created)
}
\description{
Make an .rda data object from an R object, with a particular name.
}
\examples{
makedata(".","mydat")
}

