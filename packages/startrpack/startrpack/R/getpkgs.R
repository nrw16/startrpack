#' getpkgs
#'
#' Install and load a vector of packages, if needed.
#'
#' @name getpkgs
#' @param rp A character vector of package names.
#' @return NULL
#' @export
#' @examples
#' getpkgs(c("babynames", "fueleconomy"))
getpkgs <- function(rp){
  # Check which packages are installed and loaded.
  cpi <- rp %in% rownames(installed.packages())
  cpl <- rp %in% names(sessionInfo()$otherPkgs)

  # For each package, install if not installed, and load if not loaded.
  #  Note: default behavior is to install from CRAN mirror in MI (ind=47)
  for(p in 1:length(rp)){
    if(cpi[p]){ cat(rp[p],"is already installed.\n")
      }else{ chooseCRANmirror(graphics=FALSE,ind=47); install.packages(rp[p]); cat(rp[p],"is now installed.\n")}
    if(cpl[p]){ cat(rp[p],"is already loaded.\n")
      }else{ library(rp[p],character.only=TRUE); cat(rp[p],"is now loaded.\n")}
  }
}
