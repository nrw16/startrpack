#' psremake
#'
#' Document, build, and possibly install a prexisting package.
#'
#' @name psremake
#' @param wdir A string path of a directory to find the package in.
#' @param pname A string of the package name.
#' @param check A boolean of whether or not to check the package.
#' @param install A boolean of whether or not to install the package after rebuilding.
#' @return NULL
#' @export
#' @examples
#' #psremake("~/testdir","testpackage",install=TRUE)
psremake <- function(wdir,pname,check=TRUE,install=FALSE){
  # Install required packages
  pl <- c("devtools", "roxygen2", "testthat", "knitr")
  getpkgs(pl)

  # Set working directory to folder
  sdir <- getwd()
  setwd(file.path(wdir))

  # Document
  document(pname)

  # Make the pdf manual
  pdfname <- paste(pname,".pdf",sep="")
  if(file.exists(pdfname)){file.remove(pdfname)}
  system(paste('sudo R CMD Rd2pdf',pname,'--no-preview'))

  # Build the package into a .tar.gz (with vignettes)
  ploc <- build(pname,vignettes=TRUE)

  # Check the .tar.gz as if CRAN
  if(check){
    system(paste("R CMD check --as-cran",ploc))
  }

  # Remove the package from the system
  if(pname %in% rownames(installed.packages())){remove.packages(pname)}

  # Install from the .tar.gz file onto the system
  if(install){install.packages(ploc,repos=NULL,type='source')}

  # Change directory back
  setwd(sdir)
}

