#' getaddlines
#'
#' Retrieve lines of text for the add.R function.
#'
#' @name getaddlines
#' @return A character vector of lines of text.
#' @export
#' @examples
#' cl <- getaddlines()
getaddlines <- function(){
  cl <- c("#' add")
  cl <- c(cl,"#'")
  cl <- c(cl,"#' Add together two numbers.")
  cl <- c(cl,"#'")
  cl <- c(cl,"#' @name add")
  cl <- c(cl,"#' @param x The first number to be added.")
  cl <- c(cl,"#' @param y The second number to be added.")
  cl <- c(cl,"#' @return The sum of x and y.")
  cl <- c(cl,"#' @export")
  cl <- c(cl,"#' @examples")
  cl <- c(cl,"#' add(1,1)")
  cl <- c(cl,"#' add(1,0)")
  cl <- c(cl,"add <- function(x,y){return(x+y)}")
  return(cl)
}
