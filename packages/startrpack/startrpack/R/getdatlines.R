#' getdatlines
#'
#' Retrieve lines of text for the mydat.R function.
#'
#' @name getdatlines
#' @return A character vector of lines of text.
#' @export
#' @examples
#' cl <- getdatlines()
getdatlines <- function(){
  cl <- "#' Test Data Frame."
  cl <- c(cl,"#'")
  cl <- c(cl,"#' A data frame containing some test data.")
  cl <- c(cl,"#'")
  cl <- c(cl,"#' @format A data frame with 3 rows and 2 variables:")
  cl <- c(cl,paste("#' ","\\","describe{",sep=""))
  cl <- c(cl,paste("#'   ","\\","item{A}{increasing vector}",sep=""))
  cl <- c(cl,paste("#'   ","\\","item{B}{decreasing vector}",sep=""))
  cl <- c(cl,"#'   ...")
  cl <- c(cl,"#' }")
  cl <- c(cl,paste("#' @source ","\\","url{http://www.google.com/}",sep=""))
  cl <- c(cl,'"mydat"')
  return(cl)
}
