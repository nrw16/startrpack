#' addline
#'
#' Add a new line into a file.
#'
#' @name addline
#' @param floc A text file name.
#' @param nl An integer which is the line position to add the new line after.
#' @param el A character string which is the extra line to add into the file.
#' @return NULL
#' @export
#' @examples
#' tlines <- c("test","lines")
#' wlines("./test.txt",tlines)
#' addline("./test.txt",2,"extraline")
addline <- function(floc,nl,el){
  fileConn<-file(floc)
  clines <-  readLines(fileConn)
  if(nl==0){
    clines <- c(el,clines)
  }else if(nl==length(clines)){
    clines <- c(clines,el)
  }else{
    clines <- c(clines[1:nl],el,clines[(nl+1):length(clines)])
  }
  writeLines(clines,fileConn)
  close(fileConn)
}
