#' getminusutlines
#'
#' Retrieve lines of text for the minus.R unit test.
#'
#' @name getminusutlines
#' @return A character vector of lines of text.
#' @export
#' @examples
#' cl <- getminusutlines()
getminusutlines <- function(){
  cl <- c('context("minus")','')
  cl <- c(cl,'test_that("minus function gets correct results", {','')
  cl <- c(cl,'  x <- 10; y <- 5')
  cl <- c(cl,'  expect_equal( minus(x,y), 5 )','')
  cl <- c(cl,'  x <- 50; y <- 5')
  cl <- c(cl,'  expect_equal( minus(x,y), 45 )','')
  cl <- c(cl,'})')
  return(cl)
}
